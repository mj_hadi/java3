/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparatorexample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author MohammadJavad
 */
class Student {
    int rollno;  
    String name;  
    int age; 
    Student(int rollno,String name,int age){ 
        this.rollno=rollno;  
        this.name=name;  
        this.age=age;  
    }  
}

class AgeComparator implements Comparator<Student> { 
    public int compare(Student s1,Student s2){ 
        if(s1.age==s2.age) 
            return 0;  
        else if(s1.age>s2.age)  
            return 1;  
        else  
            return -1;  
    }  
} 

class NameComparator implements Comparator<Student> { 
    
    public int compare(Student s1,Student s2){ 
        return s1.name.compareTo(s2.name);  
    }  
}

public class ComparatorExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Student> s = new ArrayList<Student>();  
        s.add(new Student(101,"Hadi",23));  
        s.add(new Student(106,"Ajay",27));  
        s.add(new Student(105,"Jai",21));  
  
        System.out.println("Sorting by Name...");  
  
        Collections.sort(s,new NameComparator());  
        for(Student st: s){ 
            System.out.println(st.rollno+" "+st.name+" "+st.age);  
        }  
        
        System.out.println("sorting by age...");  
  
        Collections.sort(s,new AgeComparator());  
        for(Student st: s){ 
            System.out.println(st.rollno+" "+st.name+" "+st.age);  
        }  
    }    
}
