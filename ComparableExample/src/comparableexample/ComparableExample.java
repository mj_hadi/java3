/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comparableexample;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author MohammadJavad
 */

class Student implements Comparable<Student> {  
    int rollno;  
    String name;  
    int age;
    Student(int rollno,String name,int age){  
        this.rollno=rollno;  
        this.name=name;  
        this.age=age;  
    }
    
    public int compareTo(Student st){  
        if(age==st.age)  
            return 0;
        else if(age>st.age)
            return 1; 
        else  
            return -1;  
    }  
}  
public class ComparableExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Student> s = new ArrayList<Student>();  
        s.add(new Student(101,"Hadi",23));  
        s.add(new Student(106,"Ajay",27));  
        s.add(new Student(105,"Jai",21));  
  
        Collections.sort(s);  
        for(Student st:s){  
        System.out.println(st.rollno+" " + st.name + " " + st.age);
        }
    }
    
}
