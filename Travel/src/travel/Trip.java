package travel;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author MJ Hadi
 */
public class Trip {
    public String destination;
    public String travellerName;
    public String travellerPassportNo;
    public String departureDate;
    public String returnDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
    
    public Trip(String destination, String travellerName, String travellerPassportNo, String departureDate, String ReturnDate) {
        setDestination(destination) ;
        setTravellerName(travellerName);
        setTravellerPassportNo(travellerPassportNo);
        setDepartureDate(departureDate);
        setReturnDate(ReturnDate);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        if (!destination.toUpperCase().matches("[a-zA-z]{2,29}")) {
            throw new IllegalArgumentException("Destination must be between 2-29 charasters long, was: " + destination);
        }
        this.destination = destination;
    }

    public String getTravellerName() {
        return travellerName;
    }

    public void setTravellerName(String travellerName) {
        if (travellerName == null) {
            throw new IllegalArgumentException("Name can't be null");

        }
        if (travellerName.length() < 2 || travellerName.length() > 20) {
            throw new IllegalArgumentException("Name must be between 2-20 charasters long");
        }
        this.travellerName = travellerName;
    }

    public String getTravellerPassportNo() {
        return travellerPassportNo;
    }

    public void setTravellerPassportNo(String travellerPassportNo) {
        if (travellerPassportNo == null) {
            throw new IllegalArgumentException("Passport Number can't be null");
        }
        if (!travellerPassportNo.toUpperCase().matches("[A-Z]{2}[0-9]{6}")) {
            throw new IllegalArgumentException("Passport Numbere must be in A-Z 123456 format, was: " + travellerPassportNo);
        }        
        this.travellerPassportNo = travellerPassportNo;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public final void setDepartureDate(String departureDate) {
        if (departureDate == null) {
            throw new IllegalArgumentException("Departure Date can't be null");
        }
        if (!isValidDate(departureDate)) {
            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd ");
        }
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public final void setReturnDate(String returnDate) {
        if (returnDate == null) {
            throw new IllegalArgumentException("Return Date can't be null");
        }
        if (!isValidDate(returnDate)) {
            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd ");
        }
//        if (!compareTwoDates(returnDate)) { 
//            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd ");
//        }             
//        this.returnDate = returnDate;
    }
    
    @Override
    public String toString() {
        return (getTravellerName() + " " + "with this passport number " + getTravellerPassportNo() + " " +
                "is travelling to " +  " " + getDestination() + " on " + getDepartureDate());
    }
    
    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());

        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
    
//    public boolean compareTwoDates(String retDate) {
//        try {
//            returnDate = dateFormat.parse(retDate);
//            if (returnDate.before(departureDate)) {
//                return false;
//            }
//            return true;
//        } catch (Exception ignore) {
//        }
//        return false;
//    }
    
}
