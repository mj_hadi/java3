
package genplay;

import java.util.ArrayList;

/**
 *
 * @author MJ Hadi
 */
public class Stack<T> {
    
    ArrayList<T> array = new ArrayList<>();    
      
    public int getHeight() { 
        return array.size();    
    }
    
    public void push(T item) {  
        // insert new element at the begining of the array
        array.add(0, item);
    }
    public T pop() {
        if ( array.isEmpty()) {
            return null;
        }
        // remove new element at the begining of the array
        return array.remove(0);    
    }
}

