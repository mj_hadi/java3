/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peopledb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MohammadJavad
 * 
 */

class SQLExceptionResultEmpty extends SQLException{}
public class Database {
    
    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "peopledb";
    private final static String USERNAME = "peopledb";
<<<<<<< HEAD
    private final static String PASSWORD = "P90uGgVS92REkWdy";
=======
    private final static String PASSWORD = "MxwcCCUuRXqYRjfc";
>>>>>>> 787ebf733f4d551474ac429446e8fe3beae3dd4b

    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);        
    }
    
<<<<<<< HEAD
    public void addPerson(Person person) throws SQLException {
        String sql = "INSERT INTO persons VALUES(?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, person.getName());
        ps.setInt(2, person.getAge());
=======
    public void addPerson(int id, String name, int age) throws SQLException {
         String query = "INSERT INTO Person VALUES(?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, id);
        ps.setString(2, name);
        ps.setInt(3, age);
>>>>>>> 787ebf733f4d551474ac429446e8fe3beae3dd4b
        ps.execute();
    }
    
    
    public ArrayList<Person> getAllPersons() throws SQLException {
        String sql = "SELECT * FROM persons";
        ArrayList<Person> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int age = result.getInt("age");
                Person person = new Person(id, name, age);
                list.add(person);
            }
        }
        return list;
    }
    
    public Person getPersonById(int id) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
    public void updatePerson(Person person) throws SQLException {
        String sql = "UPDATE persons set name=?, age=?  WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, person.getName());
            stmt.setInt(2, person.getAge());
            //where is the last parameter
            stmt.setInt(3, person.getId());            
            stmt.executeUpdate();
        }
    }
    
    public void deletePersonById(int id) throws SQLException {
        String sql = "DELETE FROM persons where id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
        stmt.setInt(1, id);
        stmt.executeUpdate();
        }
    }
    
}
