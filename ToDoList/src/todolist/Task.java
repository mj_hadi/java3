/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todolist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MJ Hadi
 */
public class Task {
    
    private String task;
    private String dueDate;  // valid date in yyyy-mm-dd format
    private boolean isDone;
    

    // ToDo enum Gender  
    public Task(String task, String dueDate, boolean isDone) {
        setTask(task);
        setDueDate(dueDate);
        this.isDone = isDone;
    }

    public String getTask() {
        return task;
    }

    public final void setTask(String task) {
       if (task.length() < 1 || task.length() > 100) {
            throw new IllegalArgumentException("Task must be between 1-100 charasters long");
        }
        this.task = task;
    }

    public String getDueDate() {
        return dueDate;
    }

    public final void setDueDate(String dueDate) {
        if (!isValidDate(dueDate)) {

            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd ");
        }
        this.dueDate = dueDate;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public final void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        if (isIsDone()) {
            return (this.getTask() + " by" + this.getDueDate() + ", Done");
        }else
        return (this.getTask() + " by" + this.getDueDate() + ", not Done");
    }
    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());

        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
}
