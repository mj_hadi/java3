/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todolist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class FileStorage {
    public static final String FILE_NAME = "data.txt";

    static void saveTaskToFile(ArrayList<Task> list) throws IOException {
        // FIXME: use try-with-resources here
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME)));
        for (Task t : list) {
            out.println(t.getTask());
            out.println(t.getDueDate());
            out.println(t.isIsDone());
        }
        out.close();
    }

    static ArrayList<Task> loadTaskFromFile() throws IOException {
        ArrayList<Task> list = new ArrayList<>();
        try {
            File file = new File(FILE_NAME);
            if (!file.exists()) {
                return list;
            }
            Scanner in = new Scanner (file);
            while (in.hasNextLine()) {
                String task = in.nextLine();
                String dueDate = in.nextLine();
                String str = in.nextLine();
                boolean isDone = Boolean.parseBoolean(str);
                in.nextLine(); //consume the left-over newline character  
                Task t = new Task(task, dueDate, isDone);
                list.add(t);
            }
        } catch (InputMismatchException e) {
            throw new IOException("Error while parsing data file", e);
        } catch (IllegalArgumentException e) {
            throw new IOException("Error while parsing data file", e);
        }
        return list;
    }
    
}
