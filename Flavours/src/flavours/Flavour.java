/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flavours;

/**
 *
 * @author MohammadJavad
 */
public class Flavour {
    
    private int flavourId;
    private String name;

    public Flavour(int flavourId, String name) {
        setFlavourId(flavourId);
        setName(name);
    }
 
    public int getFlavourId() {
        return flavourId;
    }

    public void setFlavourId(int flavourId) {
        if(flavourId < 0){
            throw new IllegalArgumentException();
        }
        this.flavourId = flavourId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty() || name.length() < 2) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return flavourId + ": " + name ;
    }
    
}
