/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flavours;

/**
 *
 * @author MohammadJavad
 */
public class PlacedOrder {
    
    private int placedOrderId;
    private String customerName;
    private String flavourList;

    public PlacedOrder(int placedOrderId, String customerName, String flavourList) {
        setPlacedOrderId(placedOrderId);
        setCustomerName(customerName);
        setFlavourList(flavourList);
    }

    public int getPlacedOrderId() {
        return placedOrderId;
    }

    public void setPlacedOrderId(int placedOrderId) {
         if(placedOrderId < 0){
            throw new IllegalArgumentException();
        }
        this.placedOrderId = placedOrderId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        if (customerName.isEmpty() || customerName.length() < 2) {
            throw new IllegalArgumentException();
        }
        this.customerName = customerName;
    }

    public String getFlavourList() {
        return flavourList;
    }

    public void setFlavourList(String flavourList) {
        if(flavourList == null){
            throw new IllegalArgumentException();
        }else if(!flavourList.matches("^[A-Za-z]+((\\s)?([A-Za-z])+)*$") || !flavourList.matches("^[A-Za-z]+((,)?([A-Za-z])+)*$")){
            throw new IllegalArgumentException();
        }
        this.flavourList = flavourList;
    }

    @Override
    public String toString() {
        return customerName + " ordered: " + flavourList;
    }
    
}
