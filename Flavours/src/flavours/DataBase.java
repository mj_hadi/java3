/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flavours;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MohammadJavad
 */
public class DataBase {
    
    private Connection conn;
    
    public DataBase() throws SQLException {
        conn = DriverManager.getConnection("jdbc:sqlite:Quiz3Database.db");
    } 
    
    public ArrayList<Flavours> getAllFlavours() throws SQLException {
        final String SELECT_ALL_REPS = "SELECT * FROM flavours";
        ArrayList<Flavours> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_REPS);
            while (rs.next()) {
                int id = rs.getInt("flavourId");
                String name = rs.getString("name");
                Flavour f = new Flavour(id, name);
                result.add(f);
            }
        }
        return result;
    }
    
    public ArrayList<PlacedOrder> getAllOrders() throws SQLException {
        final String SELECT_ALL_REPS = "SELECT * FROM placedOrder";
        ArrayList<PlacedOrder> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ALL_REPS);
            while (rs.next()) {
                int id = rs.getInt("placedOrderId");
                String name = rs.getString("customerName");
                String flavList = rs.getString("flavourList");
                PlacedOrder o = new PlacedOrder(id, name,flavList);
                result.add(o);
            }
        }
        return result;
    }
    
     public void addOrder(String name,String flavList) throws SQLException {
        String query = "INSERT INTO placedOrder VALUES (NULL, ?, ?)";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, name);
        ps.setString(2, flavList);
        ps.execute();
    }
    
}
