package todolistdb;

import java.util.Date;

/**
 *
 * @author MohammadJavad
 */
public class ToDoItem {
    private int id;
    private String title;
    private Date dueDate;
    private boolean isDone;

    public ToDoItem(int id, String title, Date dueDate, boolean isDone) {
        setId(id);
        setTitle(title);
        setDueDate(dueDate);
        setIsDone(isDone);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    @Override
    public String toString() {
        if (isIsDone()) {
            return (this.getTitle() + " by" + this.getDueDate() + ", Done");
        }else
        return (this.getTitle() + " by" + this.getDueDate() + ", not Done");
    }
    
}
