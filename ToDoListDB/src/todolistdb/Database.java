package todolistdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author MohammadJavad
 */
public class Database {
    private final static String HOSTNAME = "localhost:3306";
    private final static String DBNAME = "todolistdb";
    private final static String USERNAME = "todolistdb";
    private final static String PASSWORD = "QZrvyYEb92fa44jr";//"VmyJzvqyb8KwCE4X";

    private Connection conn;
    
    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);        
    }
    
    public void addItem(String title, java.sql.Date duedate,int done) throws SQLException {
        final String INSERT_ITEM = "INSERT INTO todoitem VALUES(null,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(INSERT_ITEM);
        ps.setString(1, title);
        ps.setDate(2, duedate);
        ps.setInt(3, done);
        ps.execute();
    }
    public ArrayList<ToDoItem> getItemsList() throws SQLException {
        final String SELECT_ITEMS = "SELECT * From todoitem";
        ArrayList<ToDoItem> result = new ArrayList<>();
        try (Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(SELECT_ITEMS);
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Date due = rs.getDate("dueDate");
                boolean done;
                if(rs.getInt("isDone") == 1){
                    done = true;
                }else{
                    done = false;
                }
                ToDoItem i = new ToDoItem(id,title,due,done);
                result.add(i);
            }
        }
        return result;
    }
    
    public void deleteItemByID(int id) throws SQLException {
        final String DELETE_ITEM = "DELETE FROM todoitem where id=" + id;
        PreparedStatement ps = conn.prepareStatement(DELETE_ITEM);
        ps.execute();
    }
    
     public void updateItemByID(int id,String title,java.sql.Date dueDate,int done) throws SQLException {
        final String UPDATE_ITEMS = "UPDATE todoitem SET title='" + title + "',dueDate='" + dueDate +"',isDone="+ done + " where id=" + id;
        PreparedStatement ps = conn.prepareStatement(UPDATE_ITEMS);
        ps.execute();
    }
    
}
