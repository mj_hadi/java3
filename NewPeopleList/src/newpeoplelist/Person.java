
package newpeoplelist;

public class Person {
    private String name;
    private int age;
    private String postalCode;
    
    Person (String name,int age,String Postal){
        setName(name);
        setAge(age);
        setPostalCode(Postal);
    }
     
    public String getName() {
        return name;
    }

    public final void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name can't be null");

        }
        if (name.length() < 2 || name.length() > 20) {
            throw new IllegalArgumentException("Name must be between 2-20 charasters long");
        }
        this.name = name;        
    }

    public int getAge() {
        return age;
    }

    public final void setAge(int age) {
        if (age < 0 || age > 150) {
            throw new IllegalArgumentException("Age must be between 0-150");
        }
        this.age = age;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        if (!postalCode.toUpperCase().matches("[A-Z][0-9][A-Z] ?[0-9][A-Z][0-9]")) {
            throw new IllegalArgumentException("Postal code must be in A1A 1A1 format, was: " + postalCode);
        }
        this.postalCode = postalCode;
    }
    
    @Override
     public String toString(){
        return String.format("%s is %d years old at %s",this.getName(),this.getAge(),this.getPostalCode());
    }
    
    
}
