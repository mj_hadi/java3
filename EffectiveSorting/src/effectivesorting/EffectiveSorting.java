package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author MJ Hadi
 */
class Person implements Comparable<Person> {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        return o.age - age;
        //return age - o.age; // Sort Nozoli
    }

}

class PersonNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.name.compareTo(o2.name);
    }
}

class PersonNameAgeComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        // test names first, if names are different - we're done
        int result = o1.name.compareTo(o2.name);
        if (result != 0) {
            return result;
        }
        // if names were identical - compare ages
        return o1.age - o2.age;
    }
}

public class EffectiveSorting {

    static ArrayList<Person> people = new ArrayList<Person>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        people.add(new Person("jerry", 33));
        people.add(new Person("arry", 3));
        people.add(new Person("jerry", 53));
        people.add(new Person("jerry", 43));
        people.add(new Person("xim", 3));
        people.add(new Person("Leila", 39));
         people.add(new Person("Nasrin", 44));

        System.out.println("====== BEFORE SORTING ======");
        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }

        Collections.sort(people);

        System.out.println("====== AFTER SORTING with Comparable ======");
        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }

        Collections.sort(people, new PersonNameComparator());

        System.out.println("====== AFTER SORTING with Comparator of names ======");
        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }

        Collections.sort(people, new PersonNameAgeComparator());

        System.out.println("====== AFTER SORTING with Comparator of names and ages ======");
        for (Person p : people) {
            System.out.printf("%s is %d y/o\n", p.name, p.age);
        }
    }
}
