
package geometry;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
public class Geometry {
    
    public static enum Shapes {
        Point,
        Rectangle,
        Square,
        Circle,
        Sphere,
        Hexagon
    }  

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Initialize an ArrayList
        ArrayList<GeoObj> geometry = new ArrayList<>();

        // Open the file
        try (BufferedReader bf = new BufferedReader(new FileReader("input.txt"))) {
            String line;
            String[] data;
            Shapes shape;
            
            // Read a line
            line = bf.readLine();

            // Start a loop
            while (line != null) {

                // Split at semicolons
                data = line.split(";");

                try {
                    // search for the shape in the enum
                    shape = Shapes.valueOf(data[0]); // First field is the shape type
                    // Continue based on a switch case
                    switch (shape) {
                        case Square:
                            // We need 2 arguments for a square in addition to the shape type for a total of 3
                            if (data.length != 4)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Square(data[1], data[2], data[3]));
                            break;
                        case Circle:
                            // We need 2 arguments for a circle in addition to the shape type for a total of 3
                            if (data.length != 3)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Circle(data[1], data[2]));
                            break;
                        case Hexagon:
                            // We need 2 arguments for a hexagon in addition to the shape type for a total of 3
                            if (data.length != 3)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Hexagon(data[1], data[2]));
                            break;
                        case Point:
                            // We need 1 arguments for a point in addition to the shape type for a total of 2
                            if (data.length != 2)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Point(data[1]));
                            break;
                        case Rectangle:
                            // We need 3 arguments for a rectangle in addition to the shape type for a total of 4
                            if (data.length != 4)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Rectangle(data[1], data[2], data[3]));
                            break;
                        case Sphere:
                            // We need 2 arguments for a circle in addition to the shape type for a total of 3
                            if (data.length != 3)
                                throw new IllegalArgumentException("Wrong number of parameters for this shape");
                            geometry.add(new geometry.Sphere(data[1], data[2]));
                            break;
                        default:
                            throw new IllegalArgumentException("Don't know what to do with this shape " + shape.toString());
                    }
                    // Create appropriate object
                } catch (IllegalArgumentException | IndexOutOfBoundsException iaeEx){
                    // Something went wrong in reading a shape definition
                    System.out.println("Something went wrong while reading line " + line + " " + iaeEx.getMessage() + ". Skipping this line.");
                }
                // Read another line
                line = bf.readLine();
            }
            //End loop
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
            System.out.println("Ooops. Something happened that prevented us from completing your request: " + ioEx.getMessage());
        }
        
        for (GeoObj g : geometry)
            System.out.println(g.print());
    }
    
}
