package geometry;

public class Hexagon extends GeoObj {
    private double edge;

    public Hexagon(String color, double edge) throws IllegalArgumentException {
        super(color);
        this.edge = edge;
    }
    
    public Hexagon(String color, String strEdge) throws IllegalArgumentException {
        super(color);
        this.edge = getPositiveDouble(strEdge);
    }
    
     public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    @Override
    public double getSurface() {
        return 3.0*Math.sqrt(3.0)/2.0*Math.pow(edge, 2);
    }

    @Override
    public double getCircumPerim() {
        return 6.0*edge;
    }

    @Override
    public int getVerticesCount() {
        return 6;
    }

    @Override
    public int getEdgesCount() {
        return 6;
    }

    @Override
    public String print() {
        return super.print() + "Hexagon{" + "edge=" + edge + '}';
    }
    
}
