
package geometry;


public abstract class GeoObj {
    private String color;
    
    public GeoObj(String color) throws IllegalArgumentException {
        
        // Check for a color with a least one letter
        if ((color == null)||(color.length()<1))
            throw new IllegalArgumentException("Color cannot be null nor empty.");
        this.color = color;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    // Utility function to return a double from a string. Throws an IllegalArguemntException which the rest of our code can handle
    static double getPositiveDouble(String strDouble) throws IllegalArgumentException {
        try {
            Double d = Double.parseDouble(strDouble);
            if (d < 0)
                throw new IllegalArgumentException("Edge sizes must be positive.");
            return d;
        } catch (NumberFormatException nfeEx) {
            throw new IllegalArgumentException("There was a problem pasing the size of an element" + nfeEx.getMessage(), nfeEx);
        }
    }
    
    abstract public double getSurface();
    
    abstract public double getCircumPerim();
    
    abstract public int getVerticesCount();
    
    abstract public int getEdgesCount();
    
    public String print() {
        return "GeoObj{" + "color=" + color + '}';
    }
    

}
