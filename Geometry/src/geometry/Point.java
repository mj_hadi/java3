package geometry;


public class Point extends GeoObj {

    public Point(String color) throws IllegalArgumentException {
        super(color);
    }

    @Override
    public double getSurface() {
        return 0;
    }

    @Override
    public double getCircumPerim() {
        return 0;
    }

    @Override
    public int getVerticesCount() {
        return 1;
    }

    @Override
    public int getEdgesCount() {
        return 0;
    }

    @Override
    public String print() {
        return super.print() + "Point{" + '}';
    }

}
