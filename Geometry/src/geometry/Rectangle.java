package geometry;


public class Rectangle extends GeoObj {
    double width, height;

    public Rectangle(String color, double width, double height) throws IllegalArgumentException {
        super(color);
        this.width = width;
        this.height = height;
    }

    public Rectangle(String color, String strEdge1, String strEdge2) throws IllegalArgumentException {
        super(color);
        this.width = GeoObj.getPositiveDouble(strEdge1);
        this.height = GeoObj.getPositiveDouble(strEdge2);
    }
    
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getSurface() {
        return width*height;
    }

    @Override
    public double getCircumPerim() {
        return 2.0*(width + height);
    }

    @Override
    public int getVerticesCount() {
        return 4;
    }

    @Override
    public int getEdgesCount() {
        return 4;
    }

    @Override
    public String print() {
        return super.print() + "Rectangle{" + "width=" + width + ", height=" + height + '}';
    }
    
}
