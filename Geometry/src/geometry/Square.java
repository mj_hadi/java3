package geometry;


public class Square extends Rectangle {
    
    private double side;    
    public Square(String color, double width, double height) throws IllegalArgumentException {
        super(color, width, height);
    }

    public Square(String color, String strWidth, String strHeight) throws IllegalArgumentException {
        super(color, getPositiveDouble(strWidth), getPositiveDouble(strHeight));
    }
    
    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        super.setWidth(side);
        super.setHeight(side);
    }   
    

    @Override
    public String print() {
        return super.print() + "Square{" + '}';
    }

    
    
}
