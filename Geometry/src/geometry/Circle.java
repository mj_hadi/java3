package geometry;

public class Circle extends GeoObj {
    private double radius;

    public Circle(String color, double radius) throws IllegalArgumentException {
        super(color);
        this.radius = radius;
    }
        
    public Circle(String color, String strRadius) throws IllegalArgumentException {
        super(color);
        this.radius = getPositiveDouble(strRadius);
    }
    
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }    
    
    @Override
    public double getSurface() {
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double getCircumPerim() {
        return 2.0*Math.PI*radius;
    }

    @Override
    public int getVerticesCount() {
        return 0; // TODO: check if this should be infinity instead.
    }

    @Override
    public int getEdgesCount() {
        return 0;
    }

    @Override
    public String print() {
        return super.print() + "Circle{" + "radius=" + radius + '}';        
    }  
    
}
