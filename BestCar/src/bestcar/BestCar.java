package bestcar;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author MJ Hadi
 */
public class BestCar {
    
    static ArrayList<Car> garage = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km)
        Car c1 = new Car("BMW", "X5", 10, 20.2, 20.3);
        Car c2 = new Car("Toyota", "Corolla", 10, 20.2, 20.3);
        Car c3 = new Car("Honda", "Cevic", 10, 20.2, 20.3);
        Car c4 = new Car("Nissan", "Rouge", 10, 20.2, 20.3);
        Car c5 = new Car("Audi", "Audi A3", 10, 20.2, 20.3);
        Car c6 = new Car("Jaguar", "Jaguar", 10, 20.2, 20.3);
        Car c7 = new Car("Kia", "Forte", 10, 20.2, 20.3);
        garage.add(c1);
        garage.add(c2);
        garage.add(c3);
        garage.add(c4);
        garage.add(c5);
        garage.add(c6);
        garage.add(c7);
        
        System.out.println("*****************Before sorting *************************");

        for (Car c : garage) {
            System.out.printf("%s is %s has %d, %.2f and %.2f \n", c.make,c.model, c.maxSpeedKmph, c.secTo100Kmph,c.litersPer100km);
        }
        
          System.out.println("*****************After  sorting by maxSpeedKmph *************************");

        Collections.sort(garage);

        for (Car str : garage) {
            System.out.println(str);
        }
      


    }
}
