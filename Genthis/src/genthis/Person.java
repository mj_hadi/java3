package genthis;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author ipd
 */
public class Person {

    private int id;
    private static int count;

    Person() {
        count++;
        id = count;
    }

    public int getUniqueId() {
        return id;
    }

    public static int getCount() {
        return count;
    }

}
