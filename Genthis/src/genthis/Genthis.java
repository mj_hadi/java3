/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genthis;

import java.util.ArrayList;
import java.util.Scanner;

public class Genthis {

    public static void main(String[] args) {
        // TODO code application logic here
//        String DATA = "Jerry White\nMarry Moe\n";
//        Scanner input = new Scanner(DATA);
//        
//        //String n1 = input.next();
//        String n1 = input.nextLine();
//        
//        System.out.printf("n1: %s", n1);
        
        try {
            // old way
            ArrayList list = new ArrayList();
            // no control over what you put into the list
            list.add("Jery was here");
            list.add(new Object());
            list.add(new Scanner(System.in));

            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                String s = (String) o;
                System.out.println(s);
            }
        } catch (ClassCastException e) {
            System.out.println("The old way belowed up: " + e.getMessage());
        }

        {
            // new way
            ArrayList<String> list = new ArrayList<>();
            list.add("Jery was here");
            //list.add(new Object());

            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                String s = (String) o;
                System.out.println(s);
            }
        }

    }
