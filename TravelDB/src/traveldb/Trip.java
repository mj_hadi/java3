package traveldb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MohammadJavad
 */
public class Trip {

    private int id;
    private String destination;
    private String name;
    private String passportNo;
    private Date departureDate;
    private Date returnDate;  
    
     private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Trip(int id, String destination, String name, String passportNo, java.sql.Date departureDateSql, java.sql.Date returnDateSql) {        
        setId(id);
        setDestination(destination);
        setName(name);
        setPassportNo(passportNo);
        setDepartureDate(departureDateSql);
        setReturnDate(returnDateSql);
    }
    
    public Trip(int id,String destination, String name, String passportNo, String departureDateStr, String returnDateStr) {        
        setId(id);
        setDestination(destination);
        setName(name);
        setPassportNo(passportNo);
        setDepartureDate(departureDateStr);
        setReturnDate(returnDateStr);
    }

    @Override
    public String toString() {
        return "Trip{" + "id=" + id + ", destination=" + destination + ", name=" + name + ", passportNo=" + passportNo + ", departureDate=" + departureDate + ", returnDate=" + returnDate + '}';
    }
        /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }     
      

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination must not be null");
        }
        if (destination.length() < 1 || destination.length() > 50) {
            throw new IllegalArgumentException("Destination must 1-50 characters long");
        }
        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name must not be null");
        }
        if (name.length() < 1 || name.length() > 50) {
            throw new IllegalArgumentException("Name must 1-50 characters long");
        }
        this.name = name;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
         String pattern = "([A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9])";
        if (!passportNo.matches(pattern)) {
            throw new IllegalArgumentException("The passport No must be in this format AB123456");
        }
        if (passportNo.length() > 8) {
            throw new IllegalArgumentException("Passport Number must 1-8 characters long.");
        }
        this.passportNo = passportNo;
    }
    
    

    public Date getDepartureDate() {
        return departureDate;
    }
    
    // translate java.util.Date into java.sql.Date
    public java.sql.Date getDepartureDateSql() {
        return new java.sql.Date(departureDate.getTime());
    }

    public String getDepartureDateString() {
        return dateFormat.format(departureDate);
    }   
    
    public final void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }   
    
    private void setDepartureDate(String departureDateStr) {
        try {
            departureDate = dateFormat.parse(departureDateStr);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Due date invalid");
        }
    }

    public Date getReturnDate() {
        return returnDate;
    }    
    // translate java.util.Date into java.sql.Date
    public java.sql.Date getReturnDateSql() {
        return new java.sql.Date(returnDate.getTime());
    }

    public String getReturnDateString() {
        return dateFormat.format(returnDate);
    }   

    public final void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }    
       
    private void setReturnDate(String returnDateStr) {
        try {
            returnDate = dateFormat.parse(returnDateStr);
        } catch (ParseException ex) {
            throw new IllegalArgumentException("Due date invalid");
        }
    }   
}
