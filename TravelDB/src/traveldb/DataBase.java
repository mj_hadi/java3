package traveldb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author MohammadJavad
 */
public class DataBase {
    
    private final static String HOSTNAME = "localhost:3333";
    private final static String DBNAME = "traveldb";
    private final static String USERNAME = "traveldb";
    private final static String PASSWORD = "VmyJzvqyb8KwCE4X";  //"QZrvyYEb92fa44jr";

    private Connection conn;
    
    public DataBase() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://" + HOSTNAME + "/" + DBNAME,
                USERNAME, PASSWORD);        
    }
    
    public void addTrip(Trip trip) throws SQLException {
        String sql = "INSERT INTO trips (destination, name, passportNo, departureDate, returnDate) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, trip.getDestination());
            stmt.setString(2, trip.getName());
            stmt.setString(3, trip.getPassportNo());
            stmt.setDate(4, trip.getDepartureDateSql());
            stmt.setDate(5, trip.getReturnDateSql());;
            
            stmt.executeUpdate();
        }
    }
    
    public ArrayList<Trip> getAllTrips() throws SQLException {
        String sql = "SELECT * FROM trips";
        ArrayList<Trip> list = new ArrayList<>();
        
        try (Statement stmt = conn.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String destination = result.getString("destination");
                String name = result.getString("name");
                String passportNo = result.getString("passportNo");
                java.sql.Date departureDateSql = result.getDate("departureDate");
                java.sql.Date returnDateSql = result.getDate("returnDate");
                Trip trip = new Trip(id,destination, name, passportNo, departureDateSql, returnDateSql);
                list.add(trip);
            }
        }
        return list;
    }
    
    public Trip getTripById(int id) throws SQLException {
        throw new RuntimeException("TODO: method not implemented");
    }
    
    public void updateTrip(Trip trip) throws SQLException {
        String sql = "UPDATE trips set destination=?, name=?, passportNo=?, departureDate=?, returnDate=? WHERE id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, trip.getDestination());
            stmt.setString(2, trip.getName());
            stmt.setString(3, trip.getPassportNo());
            stmt.setDate(4, trip.getDepartureDateSql());
            stmt.setDate(5, trip.getReturnDateSql());
            //where is the last parameter
            stmt.setInt(6, trip.getId());            
            stmt.executeUpdate();
        }
    }
    
    public void deleteTripById(int id) throws SQLException {
        String sql = "DELETE FROM trips where id=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
        stmt.setInt(1, id);
        stmt.executeUpdate();
        }
    }
    
}
