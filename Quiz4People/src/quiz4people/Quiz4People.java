
package quiz4people;

import com.sun.media.sound.InvalidDataException; 
import java.io.File; 
import java.io.IOException; 
import java.util.ArrayList; 
import java.util.Scanner; 
import java.math.BigDecimal; 
import java.util.Comparator; 
import java.util.Collections; 

/** 
     Developed by MJ Hadi
*/ 

interface Printable { 
    public void print(); // display ALL fields of this class 
} 

abstract class Person implements Printable { 
    String name; // 2 or more characters 
    int age; // 0-150 
    public Person(String name, int age) throws InvalidDataException { 
        this.age = age; 
        this.name = name; 
        if (name.length()<2){ 
            throw new InvalidDataException("Min length for Person is 2"); 
        } 
        if (age<1 && age>150){ 
            throw new InvalidDataException("age must be between 1 and 150"); 
        } 
    } 
    public void print() { 
        System.out.println(toString()); 
         
    }     
} 

class Student extends Person { 
    String program; // 2 or more characters 
    BigDecimal gpa; // from 0 to 4.0 
    public Student(String name, int age,String program, BigDecimal gpa) throws InvalidDataException { 
        super(name,age); 
        this.program = program; 
        this.gpa = gpa; 

        if (program.length()<2){ 
            throw new InvalidDataException("Min length for Program is 2"); 
        } 
        if (gpa.compareTo(BigDecimal.ZERO)<0 && gpa.compareTo( BigDecimal.valueOf(4)) > 0 ){ 
            throw new InvalidDataException("gpa must be between 0 and 4"); 
        } 
    } 
    @Override 
    public String toString() { 
        return String.format("%s is a student. He/She is %d year old and studies in program %s with gpa: %s.", name, age, program, gpa.toString()); 
    } 
} 

class Teacher extends Person { 
    String subject; // 2 or more characters 
    int yearsOfExperience; 
    public Teacher(String name, int age,String subject,int yearsOfExperience) throws InvalidDataException { 

        super(name,age); 
        this.subject = subject; 
        this.yearsOfExperience = yearsOfExperience; 

        if (subject.length()<2){ 
            throw new InvalidDataException("Min length for subject is 2"); 
        } 
    } 
    @Override 
    public String toString() { 
        return String.format("%s is a teacher. He/She is %d year old and teaches subject %s with exprience of %d year(s).", name, age, subject, yearsOfExperience); 
    } 
} 

class ParsingException extends Exception { 

    public ParsingException() { 
        super(); 
    } 

    public ParsingException(String message) { 
        super(message); 
    } 

    public ParsingException(String message, Throwable cause) { 
        super(message, cause); 
    } 
} 

/**
 *
 * @author MJ Hadi 1798064
 */
public class Quiz4People {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Person> people = new ArrayList<> (); 

        try { 
            Scanner fileInput = new Scanner(new File("input.txt")); 
            while (fileInput.hasNextLine()) { 
                String line = fileInput.nextLine(); 
                try { 
                    String[] data = line.split(";"); 
                    if (data.length != 5) { 
                        throw new ParsingException("Invalid number of fields, 5 expected"); 
                    } 
                    Person p; 
                     
                    if (new String("Student").equals(data[0])) { 
                        p = new Student(data[1], Integer.parseInt(data[2]), data[3], new BigDecimal(data[4])); 
                    } else if (new String("Teacher").equals(data[0])) { 
                        p = new Teacher(data[1], Integer.parseInt(data[2]), data[3], Integer.parseInt(data[4])); 
                    } else { 
                        System.out.println("Error parsing line: " + line); 
                        System.out.println("Unknown Type "); 
                        continue; 
                    } 

                    people.add(p); 
                } catch (ParsingException | IllegalArgumentException e) { 
                    System.out.println("Error parsing line: " + line); 
                    System.out.println("  Reason: " + e.getMessage()); 
                } 
            } 
        } catch (IOException e) { 
            /// e.printStackTrace(); 
            System.out.println("File reading error: " + e.getMessage()); 
        } 
        // 
        for (Person p : people) { 
            System.out.println(p.toString()); 
        } 
         
        System.out.println("================"); 
        System.out.println("Sorted by Age"); 
         
        Collections.sort(people, new Comparator<Person>() { 
                @Override 
                public int compare(Person p1, Person p2) { 
                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending 
                    return p1.age < p2.age ? -1 : (p1.age > p2.age ) ? 1 : 0; 
                } 
            }); 
        for (Person p : people) { 
            System.out.println(p.toString()); 
        } 
         
        System.out.println("================"); 
        System.out.println("Sorted by Name"); 
         
        Collections.sort(people, new Comparator<Person>() { 
                @Override 
                public int compare(Person p1, Person p2) { 
                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending 
                    return p1.name.toLowerCase().compareTo(p2.name.toLowerCase()); 
                } 
            }); 
        for (Person p : people) { 
            System.out.println(p.toString()); 
        } 
    }    
    
}
