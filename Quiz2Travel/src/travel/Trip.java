package travel;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MJ Hadi
 */
public class Trip {
    public String destination;
    public String name;
    public String passportNo;
    public String departureDate;
    public String returnDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
    
    public Trip(String destination, String name, String passportNo, String departureDate, String returnDate) {
        setDestination(destination) ;
        setName(name);
        setPassportNo(passportNo);
        setDepartureDate(departureDate);
        setReturnDate(returnDate);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        if (!destination.toUpperCase().matches("[a-zA-z]{1,50}")) {
            throw new IllegalArgumentException("Destination must be between 1-50 charasters long, was: " + destination);
        }
        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name can't be null");

        }
        if (name.length() < 1 || name.length() > 50) {
            throw new IllegalArgumentException("name must be between 1-50 charasters long");
        }
        this.name = name;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        if (passportNo == null) {
            throw new IllegalArgumentException("Passport Number can't be null");
        }
        if (!passportNo.toUpperCase().matches("[A-Z]{2}[0-9]{6}")) {
            throw new IllegalArgumentException("Passport Numbere must be in AZ 123456 format, was: " + passportNo);
        }        
        this.passportNo = passportNo;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public final void setDepartureDate(String departureDate) {
        if (departureDate == null) {
            throw new IllegalArgumentException("Departure Date can't be null");
        }
        if (!isValidDate(departureDate)) {
            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd ");
        }
        this.departureDate = departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public final void setReturnDate(String returnDate) {
        if (returnDate == null) {
            throw new IllegalArgumentException("Return Date can't be null");
        }
        if (!isValidDate(returnDate)) { 
            throw new IllegalArgumentException("You must enter the correct format yyyy-MM-dd");
        }
//        if (!compareTwoDates() ) {
//            throw new IllegalArgumentException("Return Date must be afer departure date.");
//        }
        this.returnDate = returnDate;
    }
    
    @Override
    public String toString() {
        return (getName() + " " + "with this passport number " + getPassportNo() + " " +
                "is travelling to " +  " " + getDestination() + " on " + getDepartureDate());
    }
    
    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());

        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
    
    public boolean compareTwoDates(String d1, String d2) throws ParseException {
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = format.parse(departureDate);
        Date date2 = format.parse(returnDate);

    try {
        if (date1.compareTo(date2) > 0) {
        return false;
    }
        return true;
    } catch (Exception ignore) {
        }
        return false;
    }
    
}
